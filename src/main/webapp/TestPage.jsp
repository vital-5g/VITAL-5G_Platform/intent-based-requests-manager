<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Video 360 Guided</title>
<link rel="stylesheet" href="IntentCSS.css">
</head>
<body>

		<div class="header">
		<a href="https://beia.eu/" target="_blank">
			<img src="WebPages/beia-logo.jpg" alt="BEIA Logo" style="height:100px"/>		
		</a>	
	</div>
	
	<div class="topnav">
		<a href="IntentPage.jsp">New Intent</a>
		<a href="GuidedSelection.jsp">Guided Selection</a>
		 <a href="https://www.vital5g.eu/" target="_blank">VITAL-5G project</a>
		 <a href="https://www.vital5g.eu/wp-content/uploads/2022/05/VITAL5G-D1.1_Report-on-Use-case-requirements-v2.0.pdf" target="_blank">VITAL-5G use cases</a>
		 <a href="HelpPage.jsp">Help</a>
   </div>
	
	<div class="row">
  		<div class="columnCreated">
  		
  			<img src="WebPages/vital-logo-web.png" alt="VITAL-5G Logo" style="height:150px; display: block; margin-left: auto; margin-right: auto; margin-top:10px; margin-bottom: 2px"/>
    		<h1>Virtual Visit Guided Selection</h1>
    		<h2>Select the attributes you want your Blueprint to have</h2>	
  	</div>
	
	<form name="intentForm" method="POST" action="Video360">
	<div class="row">
	
		<input type="hidden" name="guided" value='true'/>
  		
  		<div class="column" style="text-align: right">
  		
  				<h3>Select the country you want to run the experiment:</h3>
    			<input type="radio" name="countryToChecked" checked="checked" value="France"/>France
    			
    			<h3>Select the country you want to run the experiment from:</h3>
    			<input type="radio" name="countryFromChecked" checked="checked" value="France"/>France
    			<input type="radio" name="countryFromChecked" value="Greece"/>Greece
    			<input type="radio" name="countryFromChecked" value="Italy"/>Italy
    			<input type="radio" name="countryFromChecked" value="Spain"/>Spain
    			
    			<h3>Latency:</h3>
				<input type="number" name="latencyChecked" min="1" max="200" step="1" value="100">ms
  		</div>
  
  		<div class="column" style="text-align: left">

				<h3>Select the date you want the experiment to be executed</h3>
				Day: <input type="number" name="dayChecked" min="1" max="31" step="1" value='1'/>
				Month: <input type="number" name="monthChecked" min="1" max="12" step="1" value='1'/>
				Year: <input type="number" name="yearChecked" min="2019" max="2100" step="1" value='2019'/>
				
				<h3>Select the time you want the experiment to be executed</h3>
				Hour: <input type="number" name="hourChecked" min="0" max="23" step="1" value='0'/>
				Minutes: <input type="number" name="minutesChecked" min="0" max="59" step="1" value='0'/>
				
				<h3>Bandwidth:</h3>
				<input type="number" name="bandwidthChecked" min="1" step="1" value="50">Mbps
 		 </div>
 		 
 	</div>
 		 
 	<div class="row" style="text-align: center">
 		
 		<input type="submit" value="Submit Selection"/>
 	</div>
  	</form>
	</div>
	
	<br><br>
	<div class="footerCreated">
  			<p>Vertical Innovations in Transport And Logistics over 5G experimentation facilities</p>
	</div>
	
</body>
</html>